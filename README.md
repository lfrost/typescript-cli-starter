# typescript-cli-starter

This is a starter template for creating TypeScript command-line applications.


## Building

After cloning this repository, run `npm install`.

`npm run build`
Transpiles the TypeScript source to JavaScript that can be run in Node.js.

`runBuild`
Runs the previously built application.
